const R = require('ramda')
const { addMonths } = require('date-fns')
const { mapApiRequestLoggingEvent } = require('../src/history/utils/apiRequestEventBuilder')

test('test Mapping event to always have expected expireAt property with one month TTL', () => {
    const actual = mapApiRequestLoggingEvent(null)
    expect(actual).toBeDefined()
    expect(actual.expireAt).toBeInstanceOf(Date)
    // compare date portion only
    // account for rollover properly
    expect(actual.expireAt.toDateString()).toBe(addMonths(new Date(), 1).toDateString())
})

test('test Mapping other event property fields', () => {
    const event = {
        tracerId: 'abc12345',
        zipkinTraceId: '1234abcd',
        requestUrl: 'http://localhost',
        requestMethod: 'POST',
        requestBody: {test: 'test'},
        responseHeaders: {auth: 'authtoken'},
        responseStatus: 503,
        responseMessage: 'not found',
        responseBody: {response: 'not found'},
        startTime: '2021-08-12T16:47:39.759964Z',
        endTime: '2021-08-12T16:47:39.975752Z',
        metadata: {
            type: 'inbound'
        },
    }

    const actual = mapApiRequestLoggingEvent(event)
    const expected = R.omit('expireAt', actual)
    expect(actual).toStrictEqual(expected)
})

test('test Mapping startTime and endTime', () => {
    const event = {
        startTime: '2021-09-12T17:56:32.719Z',
        endTime: '2021-10-17'
    }
    const actual = mapApiRequestLoggingEvent(event)
    expect(actual.startTime).toBeInstanceOf(Date)
    expect(actual.endTime).toBeInstanceOf(Date)
})

test('test Mapping invalid time format', () => {
    const event = {
        startTime: 'not a time format',
    }
    const actual = mapApiRequestLoggingEvent(event)
    expect(actual.startTime).toBe('not a time format')
    expect(actual.endTime).toBe(undefined)
})