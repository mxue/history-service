const {buildQueryString} = require("../src/history/utils/apiRequestSearchQueryBuilder");

test('test buildQueryString with undefined query params', () => {
    const queryParams = undefined

    const result = buildQueryString(queryParams)
    expect(result).toStrictEqual({})
})

test('test buildQueryString with empty query params', () => {
    const queryParams = {}

    const result = buildQueryString(queryParams)
    expect(result).toStrictEqual({})
})

test('test buildQueryString with custom mappers', () => {
    const queryParams = {
        requestUrl: "^https://jpmorgan.com",
        responseStatus: 503
    }

    const result = buildQueryString(queryParams)
    expect(result).toStrictEqual({requestUrl: {'$regex': '^https://jpmorgan.com'}, responseStatus: 503})
})

test('test buildQueryString with requestUrl prepends beginning anchor', () => {
    const queryParams = {
        requestUrl: "https://jpmorgan.com",
    }

    const result = buildQueryString(queryParams)
    expect(result).toStrictEqual({requestUrl: {'$regex': '^https://jpmorgan.com'}})
})

test('test buildQueryString with default mapper', () => {
    const queryParams = {
        'metadata.type': 'INBOUND',
        zipkinTraceId: 'b0f665779043d803'
    }

    const result = buildQueryString(queryParams)
    expect(result).toStrictEqual({
        'metadata.type': 'INBOUND',
        zipkinTraceId: 'b0f665779043d803'
    })
})

test('test buildQueryString with non whitelist fields', () => {
    const queryParams = {
        fakeSearchField: 'test',
        anotherFakeField: 'test2'
    }

    const result = buildQueryString(queryParams)
    expect(result).toStrictEqual({})
})