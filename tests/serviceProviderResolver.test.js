const { resolveServiceProviderId } = require('../src/history/utils/serviceProviderResolver')

test('test resolveServiceProviderId undefined value', () => {
    expect(resolveServiceProviderId(null)).toBeUndefined()
    expect(resolveServiceProviderId(undefined)).toBeUndefined()
    expect(resolveServiceProviderId({})).toBeUndefined()
    expect(resolveServiceProviderId({
        serviceProviderIds: 'wrong value type'
    })).toBeUndefined()
    expect(resolveServiceProviderId({
        serviceProviderIds: []
    })).toBeUndefined()
})

test('test resolveServiceProviderId single serviceProviderId', () => {
    expect(resolveServiceProviderId({
        serviceProviderIds: ['cac36b73-7388-4b75-b49e-a8efcb06cdbf']
    })).toBe('cac36b73-7388-4b75-b49e-a8efcb06cdbf')
})


test('test resolveServiceProviderId multiple serviceProviderId return first', () => {
    expect(resolveServiceProviderId({
        serviceProviderIds: ['cac36b73-7388-4b75-b49e-a8efcb06cdbf', 'dasdgas-7388-4b75-b49e-a8efcb06cdbf']
    })).toBe('cac36b73-7388-4b75-b49e-a8efcb06cdbf')
})