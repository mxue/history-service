#History Service

This service will capture events from different sources and persist them to mongoDB.

TODO: Add description of all events and endpoints

## Api Request Logs

The history service implements a rabbitMQ listener that handles `ApiRequestLoggingEvent` from rabbitMQ and persists them in the `history-db` `apiRequestLogs` collection of mongoDB.

There are currently two `type`s of events,
OUTBOUND: requests sent from Fispan to external services such as banking integrations
INBOUND: request received by Fispan through the `api-proxy-server`

They are differentiated in `metadata.type` of the persisted documents.

Documents persisted with a TTL of 1 month and deleted after they expire. 

The search api allows querying from the endpoint GET `history-service/api-request-logs/search`.

query parameters restricted to a set of whitelisted fields:
'requestUrl',
'metadata.type',
'metadata.clientId',
'metadata.paymentRequestId',
'metadata.serviceProviderId',
'responseStatus',
'zipkinTraceId',
'tracerId',
'requestMethod'

You may add query params to the search request to filter search results, example: `history-service/api-request-logs/search?responseStatus=503&requestUrl=https://jpmorgan.com`

This will search for all request that were sent to `https://jpmorgan.com` with a response status of 503.

There is further performance restrictions on `requestUrl` as it uses regex matching. The above query will match any request that were sent to a path starting with `https://jpmorgan.com` with the effective regex expression of `^https://jpmorgan.com`

### Searching with just `jpmorgan.com` will not return results as the url must have the full prefix