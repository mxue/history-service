const express = require('express')
const expressWinston = require('express-winston')
const compression = require('compression')
const helmet = require('helmet')
const bodyParser = require('body-parser')

const {
    logger,
    connectMongo,
    incidentReportingMiddleWare,
    authMiddleware,
    healthCheckMiddleware,
    expressTracerMiddleware,
    expressPrincipalMiddleware,
} = require('@fispan/commons-node')

const { consumeHistory } = require('./history/queue/subscribeToEventBus')
const {
    declareMoneyTransferRoutes,
    declareAuditLogsRoutes,
    declarePaymentRequestRoutes,
    declarePaymentStatusRoutes,
    declareMigrationRoutes,
    declareApiLoggingRoutes,
} = require('./history/api/declareRoutes')
const migrations = require('./history/effects/migrations')

// Constants
const PORT = process.env.PORT || 9400
const HOST = process.env.HOST || '0.0.0.0'
const HEALTH = '/health'
const ANALYTICS_EVENT = '/fispan-data/event'

// Create the basic app
const app = express()
app.use(bodyParser.json({ limit: '10mb' }))

if (process.env.NODE_ENV === 'production') {
    app.use(compression())
}

logger.info(`[Server Startup] Wait for: [${process.env.WAIT_FOR}]`)

app.use(helmet())

expressTracerMiddleware(app)





app.use(expressWinston.logger({
    winstonInstance: logger,
    statusLevels: true,
    msg: '[Server] HTTP {{res.statusCode}} {{req.method}} {{req.url}}',
    meta: false,
    ignoreRoute: ({ url }) => url === HEALTH || url === ANALYTICS_EVENT,
}))

app.use(authMiddleware)

// Configure health check
app.use(HEALTH, healthCheckMiddleware())


// Setting Middleware for principal logging
expressPrincipalMiddleware(app)


// Init application
app.promiseListen = function promiseListen(port, host) {
    return new Promise((resolve, reject) =>
        this.listen(port, host, err => (!err ? resolve() : reject(err))))
}

app.set('json spaces', 2)

async function runServer() {
    // connect to MongoDB Atlas
    await connectMongo('history-db')

    // Migrate database
    const migrationClient = migrations()
    const pendingMigrations = await migrationClient.pending()

    logger.info(`[Server Startup] Starting database migration (${pendingMigrations.length} pending)`)
    await migrationClient.up()
    logger.info('[Server Startup] Finished database migration')

    // subscribe to event bus
    consumeHistory().catch(error => logger.error('Failed to consumeHistory', { error }))

    // declare routes
    declareMoneyTransferRoutes(app)
    declareAuditLogsRoutes(app)
    declarePaymentRequestRoutes(app)
    declarePaymentStatusRoutes(app)
    declareMigrationRoutes(app)
    declareApiLoggingRoutes(app)

    // error handling
    incidentReportingMiddleWare(app)

    // Start server
    await app.promiseListen(PORT, HOST)
    logger.info(`new [Server Startup] Running on http://${HOST}:${PORT}`)
}

runServer().catch(e =>
    logger.error(`[Server] Unexpected error found! ${e.message}\n%o`, e))
