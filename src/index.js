/* eslint-disable import/no-extraneous-dependencies,global-require */
const path = require('path')
// Read .env on development machines
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config({ path: path.resolve(process.env.HOME, 'fispan2/.envdir/history-service.env') })
}
const { waitForDependencies, waitForSecrets } = require('@fispan/commons-node')

const onReady = () => {
    require('./server')
}
const secretNames = [
    '/DATABASE_CREDENTIALS/postgres/generic',
    '/DATABASE_CREDENTIALS/mongo/generic',
    '/JWT_CREDENTIALS/internalJWTCredentials',
]
waitForDependencies(waitForSecrets(secretNames, onReady))
