exports.up = mongo => mongo().collection('apiRequestLogs').createIndex( { "expireAt": 1 }, { expireAfterSeconds: 0 })

exports.down = mongo => mongo.collection('apiRequestLogs').dropIndexes()
