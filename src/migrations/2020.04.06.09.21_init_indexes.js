exports.up = mongo => Promise.all([
    mongo().collection('changeSets').createIndexes([
        { name: 'serviceProviderIdEntityIdIndex', key: { serviceProviderId: 1, entityId: 1 } },
        { name: 'clientIdEntityIdIndex', key: { clientId: 1, entityId: 1 } },
        { name: 'entityIdTimestampIndex', key: { entityId: 1, timestamp: -1 } },
    ]),
    mongo().collection('moneyTransferLogs').createIndex({ moneyTransferId: 1 }, { name: 'moneyTransferIdIndex' }),
    mongo().collection('paymentStatusLogs').createIndex({ paymentRequestId: 1 }, { name: 'paymentStatusIdIndex' }),
    mongo().collection('paymentRequestLogs').createIndexes([
        { name: 'paymentRequestIdIndex', key: { paymentRequestId: 1 } },
        { name: 'paymentRequestClientIdTimestampIndex', key: { clientId: 1, timestamp: -1 } },
        { name: 'paymentRequestServiceProviderIdTimestampIndex', key: { serviceProviderId: 1, timestamp: -1 } },
    ]),
])

exports.down = mongo => Promise.all([
    mongo().collection('changeSets').dropIndexes(['clientIdEntityIdIndex', 'entityIdTimestampIndex', 'serviceProviderIdEntityIdIndex']),
    mongo().collection('moneyTransferLogs').dropIndex('moneyTransferIdIndex'),
    mongo().collection('paymentStatusLogs').dropIndex('paymentStatusIdIndex'),
    mongo().collection('paymentRequestLogs').dropIndexes(['paymentRequestIdIndex', 'paymentRequestClientIdTimestampIndex', 'paymentRequestServiceProviderIdTimestampIndex']),
])
