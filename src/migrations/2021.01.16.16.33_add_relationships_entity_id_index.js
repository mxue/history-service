exports.up = mongo => mongo().collection('changeSets').createIndex({ 'relationships.entityId': 1 }, { name: 'relationshipsEntityIdIndex' })

exports.down = mongo => mongo().collection('changeSets').dropIndex('relationshipsEntityIdIndex')
