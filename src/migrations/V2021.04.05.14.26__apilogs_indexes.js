exports.up = mongo => mongo().collection('apiRequestLogs').createIndexes([
    { name: 'startTimeIndex', key: { startTime: 1 } },
    { name: 'tracerIdIndex', key: { tracerId: 1 } },
])

exports.down = mongo => mongo().collection('apiRequestLogs').dropIndexes(['startTimeIndex', 'tracerIdIndex'])
