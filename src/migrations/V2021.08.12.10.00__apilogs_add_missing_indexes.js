exports.up = mongo => mongo().collection('apiRequestLogs').createIndexes([
    { name: 'responseStatusIndex', key: { 'responseStatus': 1 } },
    { name: 'zipkinTraceIdIndex', key: { 'zipkinTraceId': 1 } },
    { name: 'requestMethodIndex', key: { 'requestMethod': 1 } },
])

exports.down = mongo => mongo().collection('apiRequestLogs')
    .dropIndexes(['responseStatusIndex', 'zipkinTraceIdIndex', 'requestMethodIndex'])
