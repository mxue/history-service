exports.up = mongo => mongo().collection('apiRequestLogs').createIndexes([
    { name: 'requestUrlIndex', key: { requestUrl: 1 } }
])

exports.down = mongo => mongo().collection('apiRequestLogs').dropIndexes(['requestUrlIndex'])
