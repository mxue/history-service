exports.up = mongo => mongo().collection('apiRequestLogs').createIndexes([
    { name: 'metadataTypeIndex', key: { 'metadata.type': 1 } },
    { name: 'metadataClientIdIndex', key: { 'metadata.clientId': 1 } },
    { name: 'metadataPaymentRequestIdIndex', key: { 'metadata.paymentRequestId': 1 } },
    { name: 'metadataServiceProviderIdIndex', key: { 'metadata.serviceProviderId': 1 } }
])

exports.down = mongo => mongo().collection('apiRequestLogs')
    .dropIndexes(['metadataTypeIndex','metadataClientIdIndex', 'metadataPaymentRequestIdIndex', 'metadataServiceProviderIdIndex'])
