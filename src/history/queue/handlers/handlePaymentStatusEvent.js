const R = require('ramda')
const { getMessageFromEvent, logType } = require('../../utils/queueUtils')
const { persistPaymentStatusLog, getLatestPaymentStatusLogs } = require('../../effects/paymentStatusLog')

module.exports = async (event) => {
    const eventPaymentStatusLog = R.pipe(
        logType({}),
        getMessageFromEvent,
        R.applySpec({
            paymentRequestId: R.prop('paymentRequestId'),
            moneyTransferId: R.prop('moneyTransferId'),
            endToEndId: R.prop('endToEndId'),
            clientId: R.prop('clientId'),
            createdDate: () => new Date(),
            lastUpdatedDate: () => new Date(),
            fullStatusJson: R.pipe(R.prop('fullStatusJson'), JSON.parse),
        })
    )(event)
    const latestPaymentStatusLog = await R.pipe(
        R.prop('paymentRequestId'),
        R.objOf('paymentRequestId'),
        getLatestPaymentStatusLogs,
        R.then(
            R.pipe(
                R.unless(
                    R.isNil,
                    R.applySpec({
                        paymentRequestId: R.prop('paymentRequestId'),
                        moneyTransferId: R.prop('moneyTransferId'),
                        endToEndId: R.prop('endToEndId'),
                        clientId: R.prop('clientId'),
                        createdDate: R.prop('createdDate'),
                    }),
                ),
                R.mergeRight(eventPaymentStatusLog),
            )
        )
    )(eventPaymentStatusLog)

    return persistPaymentStatusLog(latestPaymentStatusLog)
}
