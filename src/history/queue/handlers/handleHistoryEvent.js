const R = require('ramda')
const { logger } = require('@fispan/commons-node')
const { getMessageFromEvent, hasNonExternalIdChanges } = require('../../utils/queueUtils')
const { getLatestChangeSet, persistChangeSet } = require('../../effects/auditLog')

module.exports = async (historyEvent) => {
    const { changeSet } = getMessageFromEvent(historyEvent)
    const { event, clazz, entityId, changedBy } = changeSet

    const latestChangeSet = await getLatestChangeSet({ entityId })

    return R.pipe(
        R.prop('_id'),
        R.defaultTo(null),
        R.assoc('parentId', R.__,
            R.applySpec({
                entityId: R.prop('entityId'),
                serviceProviderId: R.prop('serviceProviderId'),
                clientId: R.prop('clientId'),
                event: R.prop('event'),
                changedBy: R.prop('changedBy'),
                jsonSnapshot: R.pipe(R.prop('jsonSnapshot'), JSON.parse),
                timestamp: message => new Date(R.prop('timestamp', message)),
                entityType: R.prop('clazz'),
                relationships: R.pipe(
                    R.prop('relationships'),
                    R.map(R.applySpec({
                        entity: R.prop('entityClassName'),
                        has: R.prop('hasEntityName'),
                        entityId: R.prop('entityId'),
                        hasId: R.prop('hasId'),
                        status: R.prop('status'),
                        timestamp: message => new Date(R.prop('timestamp', message)),
                    })),
                    R.when(R.isEmpty, () => null)
                ),
            })(changeSet)
        ),
        R.when(
            R.pipe(
                R.prop('jsonSnapshot'),
                hasNonExternalIdChanges(R.prop('jsonSnapshot', latestChangeSet)),
            ),
            R.pipe(
                R.tap(
                    () => logger.info(`[Event Bus] Audit Event: ${event}:${clazz}, id: ${entityId}, by: ${changedBy}`)
                ),
                persistChangeSet,
            )
        )
    )(latestChangeSet)
}
