const R = require('ramda')
const { getMessageFromEvent, logType } = require('../../utils/queueUtils')
const { persistMoneyTransferLog } = require('../../effects/moneyTransferLog')

module.exports = async event =>
    R.pipe(
        logType({}),
        getMessageFromEvent,
        R.applySpec({
            status: R.prop('status'),
            timestamp: message => new Date(R.prop('eventDate', message)),
            message: R.prop('message'),
            moneyTransferId: R.prop('moneyTransferId'),
            statusMessages: R.prop('messages'),
        }),
        persistMoneyTransferLog,
    )(event)
