const R = require('ramda')
const {mapApiRequestLoggingEvent} = require("../../utils/apiRequestEventBuilder")
const {getMessageFromEvent, logType} = require("../../utils/queueUtils")
const {persistAPIRequest} = require('../../effects/apiRequestLog')

module.exports = async (event) => {
    const apiRequestEvent = R.pipe(
        logType({}),
        getMessageFromEvent,
        mapApiRequestLoggingEvent
    )(event)
    return persistAPIRequest(apiRequestEvent)
}
