const R = require('ramda')
const { getMessageFromEvent, logType } = require('../../utils/queueUtils')
const { persistPaymentRequestLog } = require('../../effects/paymentRequestLog')

module.exports = async event =>
    R.pipe(
        logType({}),
        getMessageFromEvent,
        R.applySpec({
            status: R.prop('newStatus'),
            timestamp: message => new Date(R.prop('eventDate', message)),
            message: R.prop('message'),
            serviceProviderId: R.prop('serviceProviderId'),
            paymentRequestId: R.prop('paymentRequestId'),
            clientId: R.prop('clientId'),
            statusMessages: R.prop('messages'),
        }),
        persistPaymentRequestLog,
    )(event)
