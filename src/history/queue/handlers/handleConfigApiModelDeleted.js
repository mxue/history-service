const removeCollections = require('../../effects/removeCollections')

module.exports = async () => removeCollections()
