const { EventConsumer } = require('@fispan/commons-node')
const { EVENT: {
    HISTORY_EVENT,
    CONFIG_API_MODEL_DELETED,
    MONEY_TRANSFER_LOG,
    PAYMENT_REQUEST_CHANGED,
    PAYMENT_STATUS_EVENT,
    API_REQUEST_LOGGING_EVENT,
} } = require('../utils/constants')
const handleHistoryEvent = require('./handlers/handleHistoryEvent')
const handleConfigModelDeleted = require('./handlers/handleConfigApiModelDeleted')
const handleMoneyTransferLog = require('./handlers/handleMoneyTransferLog')
const handlePaymentRequest = require('./handlers/handlePaymentRequest')
const handlePaymentStatus = require('./handlers/handlePaymentStatusEvent')
const handleApiLogging = require('./handlers/handleApiRequestLogging')

module.exports.consumers = [
    EventConsumer([HISTORY_EVENT], handleHistoryEvent),
    EventConsumer([CONFIG_API_MODEL_DELETED], handleConfigModelDeleted),
    EventConsumer([MONEY_TRANSFER_LOG], handleMoneyTransferLog),
    EventConsumer([PAYMENT_REQUEST_CHANGED], handlePaymentRequest),
    EventConsumer([PAYMENT_STATUS_EVENT], handlePaymentStatus),
    EventConsumer([API_REQUEST_LOGGING_EVENT], handleApiLogging),
]
