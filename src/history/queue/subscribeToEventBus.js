const { consumeQueue } = require('@fispan/commons-node')
const { consumers } = require('./eventsRouter')
const { QUEUE_PREFETCH_COUNT } = require('../utils/constants')

const consumeHistory = () => consumeQueue({
    consumers,
    prefetch: QUEUE_PREFETCH_COUNT,
})

module.exports = {
    consumeHistory,
}
