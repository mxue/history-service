const { Router } = require('express')
const { logger } = require('@fispan/commons-node')
const { searchForPaymentRequestsById, getAllPaymentRequestsSinceTimestamp } = require('./endpoints/paymentRequest')
const { searchAPIEventByParams } = require('./endpoints/apiRequestLogging')
const { findPaymentStatusById } = require('./endpoints/paymentStatus')
const {
    searchLatestChangeSets,
    calculateFieldChange,
} = require('./endpoints/auditLog')
const { searchForMoneyTransfersAllById } = require('./endpoints/moneyTransfer')
const migrateRecords = require('./endpoints/migrations')
const { getDeclaredRoutes } = require('../utils/responseUtils')
const { doAsClientOrServiceProviderAtLeast, doAsOperationsAtLeast } = require('../utils/responseUtils')

const routerOptions = {
    mergeParams: true,
    strict: true,
}

const declareAuditLogsRoutes = (app) => {
    const route = '/audit-logs'
    const router = Router(routerOptions)

    router.get('/', doAsClientOrServiceProviderAtLeast, searchLatestChangeSets)

    router.get('/:id', doAsClientOrServiceProviderAtLeast, calculateFieldChange)

    app.use(route, router)

    logger.info(`[API] audit log routes registered:\n\t${getDeclaredRoutes(route, router)}`)
}

const declarePaymentRequestRoutes = (app) => {
    const route = '/payment-request-logs'
    const router = Router(routerOptions)

    router.get('/search/by-payment-request-id', doAsClientOrServiceProviderAtLeast, searchForPaymentRequestsById)

    router.get('/', doAsClientOrServiceProviderAtLeast, getAllPaymentRequestsSinceTimestamp)

    app.use(route, router)

    logger.info(`[API] payment request log routes registered:\n\t${getDeclaredRoutes(route, router)}`)
}

const declarePaymentStatusRoutes = (app) => {
    const route = '/payment-status-logs'
    const router = Router(routerOptions)

    router.get('/search/by-payment-request-id', doAsClientOrServiceProviderAtLeast, findPaymentStatusById)

    app.use(route, router)

    logger.info(`[API] payment status log routes registered:\n\t${getDeclaredRoutes(route, router)}`)
}

const declareMoneyTransferRoutes = (app) => {
    const route = '/money-transfer-logs'
    const router = Router(routerOptions)

    router.get(
        '/search/search-all-by-money-transfer-id',
        doAsClientOrServiceProviderAtLeast,
        searchForMoneyTransfersAllById,
    )

    app.use(route, router)

    logger.info(`[API] money transfer logs routes registered:\n\t${getDeclaredRoutes(route, router)}`)
}

const declareMigrationRoutes = (app) => {
    const route = '/migration'
    const router = Router(routerOptions)

    router.post('/:table', doAsOperationsAtLeast, migrateRecords.validate, migrateRecords.perform)

    app.use(route, router)

    logger.info(`[API] migration routes registered:\n\t${getDeclaredRoutes(route, router)}`)
}

const declareApiLoggingRoutes = (app) => {
    const route = '/api-request-logs'
    const router = Router(routerOptions)

    router.get('/search', doAsOperationsAtLeast, searchAPIEventByParams)

    app.use(route, router)

    logger.info(`[API] api logs routes registered:\n\t${getDeclaredRoutes(route, router)}`)
}


module.exports = {
    declareApiLoggingRoutes,
    declareAuditLogsRoutes,
    declareMigrationRoutes,
    declareMoneyTransferRoutes,
    declarePaymentRequestRoutes,
    declarePaymentStatusRoutes,
}
