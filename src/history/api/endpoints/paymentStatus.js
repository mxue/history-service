const R = require('ramda')
const { respondWith: { ok, serverErrorAndReport, notFound } } = require('../../utils/responseUtils')
const { getLatestPaymentStatusLogs } = require('../../effects/paymentStatusLog')

const findPaymentStatusById = async (req, res) => {
    try {
        const id = R.path(['query', 'id'], req)
        const queryResults = await getLatestPaymentStatusLogs({ paymentRequestId: id })

        return R.isNil(queryResults)
            ? notFound(res, 'payment status log not found')
            : ok(res, queryResults)
    } catch (e) {
        return serverErrorAndReport(res, 'Could not find payment status logs', e)
    }
}

module.exports = {
    findPaymentStatusById,
}
