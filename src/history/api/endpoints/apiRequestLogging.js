const {sanitizeObject} = require('@fispan/sanitize')
const R = require('ramda')
const {respondWith: {ok, serverErrorAndReport, notFound}, getPageable} = require('../../utils/responseUtils')
const {searchApiRequestLogs} = require('../../effects/apiRequestLog')
const url = require('url');
const {buildQueryString} = require("../../utils/apiRequestSearchQueryBuilder");

const sanitizeAndSend = (res, queryResults) => {
    const sanitizedResults = R.forEach(sanitizeObject, queryResults)
    return ok(res, sanitizedResults)
}

const searchAPIEventByParams = async (req, res) => {
    try {
        const {page, size} = getPageable(req)
        const queryParams = url.parse(req.url, true).query
        const query = buildQueryString(queryParams)
        const queryResults = await searchApiRequestLogs(query, page, size)

        return R.isNil(queryResults)
            ? notFound(res, 'api request log not found')
            : sanitizeAndSend(res, queryResults)
    } catch (e) {
        return serverErrorAndReport(res, 'Could not find api request logs', e)
    }
}

module.exports = {
    searchAPIEventByParams,
}
