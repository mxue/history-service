const R = require('ramda')
const EventEmitter = require('events')
const { logger, createPostgresClient, getMongoDb: mongoDbDriver } = require('@fispan/commons-node')
const { respondWith: { accepted, serverErrorAndReport } } = require('../../../history/utils/responseUtils')
const { validateDates } = require('../../../history/utils/validationUtils')

const migrationEmitter = new EventEmitter()
const postgresDriver = createPostgresClient()

const DATE_RANGE_PARTITION_SIZE = 7 * 24 * 60 * 60 * 1000 // 7 days
const MIGRATION_EVENTS = { NEXT: 'NEXT', DONE: 'DONE' }

const getMongoCollectionName = table =>
    R.prop(
        table,
        {
            change_set: 'changeSets',
            money_transfer_log: 'moneyTransferLogs',
            payment_request_log: 'paymentRequestLogs',
        }
    )

const logMigrationFinished = ({ earlierDateRange, laterDateRange, table, totalRecordsPersisted }) => {
    // eslint-disable-next-line max-len
    logger.info(`[Migration] ${totalRecordsPersisted} records in ${table} from ${earlierDateRange} to ${laterDateRange} successfully migrated`)
}

const persistRecords = async (payload, table) =>
    mongoDbDriver().collection(table)
        .insertMany(payload)

const getRecordsInRange = (earlierDateRange, laterDateRange, table, recordLimit) => postgresDriver(table)
    .select(
        postgresDriver.raw(
            `*, count(*) OVER() AS total_count${table === 'change_set'
                // eslint-disable-next-line max-len
                ? ', (SELECT json_agg(relationship_set) FROM relationship_set WHERE change_set.id = relationship_set.change_set_id) as relationships'
                : ''}`
        )
    )
    .where('timestamp', '>=', earlierDateRange)
    .andWhere('timestamp', '<', laterDateRange)
    .limit(recordLimit)
    .orderBy('timestamp', 'DESC')

const isMigrationCompleted = (dateMigrated, earlierDateRange, remainingRecords, recordLimit) =>
    R.and(
        R.equals(dateMigrated, earlierDateRange),
        R.gte(parseInt(recordLimit, 10), parseInt(remainingRecords, 10))
    )

// Will limit date range of queries to 1 week, to speed up queries and reduce db load
const getDateBoundary = (earlierDateRange, laterDateBoundary) => {
    const earlierDateRangeObject = new Date(earlierDateRange)
    const laterDateBoundaryObject = new Date(laterDateBoundary)
    const earlierDateBoundary = new Date(laterDateBoundaryObject.getTime() - DATE_RANGE_PARTITION_SIZE)

    return earlierDateBoundary < earlierDateRangeObject
        ? earlierDateRange
        : earlierDateBoundary.toISOString()
}

const formatRecords = records =>
    R.map(
        R.pipe(
            R.omit(['id', 'totalCount']),
            R.evolve({
                relationships: R.unless(R.isNil, R.map(R.omit(['id', 'change_set_id']))),
                jsonSnapshot: JSON.parse,
                fullStatusJson: JSON.parse,
            })
        )
    )(records)

const migrateRecordsInRange = async (earlierDateRange, laterDateBoundary, table, recordLimit) => {
    const earlierDateBoundary = getDateBoundary(earlierDateRange, laterDateBoundary)
    const existingRecords = await getRecordsInRange(earlierDateBoundary, laterDateBoundary, table, recordLimit)

    if (R.isEmpty(existingRecords)) {
        return {
            isCompleted: isMigrationCompleted(earlierDateBoundary, earlierDateRange, 0, recordLimit),
            latestPersistedRecord: earlierDateBoundary,
            amountPersisted: 0,
        }
    }
    await persistRecords(formatRecords(existingRecords), getMongoCollectionName(table))

    const remainingRecordCount = R.pipe(R.head, R.prop('totalCount'))(existingRecords)
    const isCompleted = isMigrationCompleted(earlierDateBoundary, earlierDateRange, remainingRecordCount, recordLimit)
    const latestPersistedRecord = R.pipe(R.last, R.prop('timestamp'))(existingRecords)
    return { isCompleted, latestPersistedRecord, amountPersisted: R.length(existingRecords) }
}

migrationEmitter.on(
    MIGRATION_EVENTS.NEXT,
    async ({ earlierDateRange, laterDateBoundary, laterDateRange, table, recordLimit, totalRecordsPersisted = 0 }) => {
        const {
            isCompleted,
            latestPersistedRecord,
            amountPersisted,
        } = await migrateRecordsInRange(earlierDateRange, laterDateBoundary, table, recordLimit)

        // eslint-disable-next-line max-len
        logger.info(`[Migration] successfully migrated ${totalRecordsPersisted + amountPersisted} records up to ${latestPersistedRecord}`)

        return isCompleted
            ? migrationEmitter.emit(
                MIGRATION_EVENTS.DONE,
                {
                    earlierDateRange,
                    laterDateRange,
                    table,
                    totalRecordsPersisted: totalRecordsPersisted + amountPersisted,
                })
            : setTimeout(() => migrationEmitter.emit(
                MIGRATION_EVENTS.NEXT,
                {
                    earlierDateRange,
                    laterDateBoundary: latestPersistedRecord,
                    laterDateRange,
                    table,
                    recordLimit,
                    totalRecordsPersisted: totalRecordsPersisted + amountPersisted,
                }), 300)
    }
)

migrationEmitter.on(MIGRATION_EVENTS.DONE, logMigrationFinished)

const migrateRange = {
    perform: async (req, res) => {
        try {
            const { earlierDateRange, laterDateRange, recordLimit = 100 } = R.prop('query', req)
            const { table } = R.prop('params', req)
            migrationEmitter.emit(
                MIGRATION_EVENTS.NEXT,
                { earlierDateRange, laterDateBoundary: laterDateRange, laterDateRange, table, recordLimit }
            )

            // eslint-disable-next-line max-len
            return accepted(res, `Requested records from ${earlierDateRange} to ${laterDateRange} in ${table}, with record persist limit ${recordLimit}`)
        } catch (e) {
            return serverErrorAndReport(res, 'Could not migrate records in range', e)
        }
    },

    validate: (req, res, next) => validateDates(['earlierDateRange', 'laterDateRange'])(req, res, next),
}

module.exports = migrateRange
