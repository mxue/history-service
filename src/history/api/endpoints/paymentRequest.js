const R = require('ramda')
const { respondWith: { accepted, serverErrorAndReport }, getPageable } = require('../../utils/responseUtils')
const { searchPaymentRequestLogs } = require('../../effects/paymentRequestLog')
const { withJailing } = require('../../utils/databaseUtils')

const searchForPaymentRequestsById = async (req, res) => {
    try {
        const { page, size } = getPageable(req)
        const id = R.path(['query', 'id'], req)
        const queryResults = await searchPaymentRequestLogs({ paymentRequestId: id }, page, size)

        return accepted(
            res,
            'Found payment requests',
            R.applySpec({
                _embedded: R.objOf('paymentRequestLogs'),
                page: R.applySpec({
                    size: R.always(size),
                    totalElements: R.length,
                    number: R.always(page),
                }),
            })(queryResults)
        )
    } catch (e) {
        return serverErrorAndReport(res, 'Could not find payment request logs', e)
    }
}

const getAllPaymentRequestsSinceTimestamp = async (req, res) => {
    try {
        const { page, size } = getPageable(req)
        const dateTimeInput = new Date(R.path(['query', 'since'], req))
        const queryResults = await searchPaymentRequestLogs(
            withJailing(req, { timestamp: { $gte: dateTimeInput } }),
            page,
            size,
        )

        return accepted(
            res,
            'Found payment requests',
            R.applySpec({
                _embedded: R.objOf('paymentRequestLogs'),
                page: R.applySpec({
                    size: R.always(size),
                    totalElements: R.length,
                    number: R.always(page),
                }),
            })(queryResults)
        )
    } catch (e) {
        return serverErrorAndReport(res, 'Could not find payment request logs', e)
    }
}

module.exports = {
    getAllPaymentRequestsSinceTimestamp,
    searchForPaymentRequestsById,
}
