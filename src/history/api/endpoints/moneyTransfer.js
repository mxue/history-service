const R = require('ramda')
const { respondWith: { accepted, serverErrorAndReport }, getPageable } = require('../../utils/responseUtils')
const { searchMoneyTransferLogs } = require('../../effects/moneyTransferLog')

const searchForMoneyTransfersAllById = async (req, res) => {
    try {
        const { page, size } = getPageable(req)
        const id = R.path(['query', 'id'], req)
        const queryResults = await searchMoneyTransferLogs({ moneyTransferId: id }, page, size)

        return accepted(
            res,
            'Found money transfer logs',
            R.applySpec({
                _embedded: R.objOf('moneyTransferLogs'),
                page: R.applySpec({
                    size: R.always(size),
                    totalElements: R.length,
                    number: R.always(page),
                }),
            })(queryResults)
        )
    } catch (e) {
        return serverErrorAndReport(res, 'Could not find money transfer logs', e)
    }
}

module.exports = {
    searchForMoneyTransfersAllById,
}
