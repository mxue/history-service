const R = require('ramda')
const { respondWith: { ok, serverErrorAndReport }, getPageable, jsonPath } = require('../../utils/responseUtils')
const symmetricDifference = require('../../utils/symmetricDifference')
const { getLatestChangeSetsWithPaging, getLatestChangeSets } = require('../../effects/auditLog')
const { withJailing } = require('../../utils/databaseUtils')

const searchLatestChangeSets = async (req, res) => {
    try {
        const { page, size } = getPageable(req)
        const queryResults = await getLatestChangeSetsWithPaging(withJailing(req), page, size)

        return ok(
            res,
            queryResults,
        )
    } catch (e) {
        return serverErrorAndReport(res, 'Could not find audit logs', e)
    }
}

const calculateFieldChange = async (req, res) => {
    try {
        const id = R.path(['params', 'id'], req)
        const changeSets = await getLatestChangeSets(
            withJailing(
                req,
                { $or: [{ entityId: id }, { 'relationships.entityId': id }] }
            )
        )

        const formatChanges = set => R.addIndex(R.reduce)((changes, { timestamp, changedBy }, index) =>
            R.pipe(
                R.prepend({}), // Acts as an initial comparison for the first change
                R.converge(
                    symmetricDifference,
                    [
                        R.pipe(R.nth(index), R.prop('jsonSnapshot')),
                        R.pipe(R.nth(index + 1), R.prop('jsonSnapshot')),
                        () => [R.path([index, 'entityType'], set)],
                    ]
                ),
                R.map(R.pipe(
                    R.assoc('timestamp', timestamp),
                    R.assoc('changedBy', changedBy),
                    R.evolve({ propertyName: jsonPath }))),
                R.concat(changes),
            )(set),
        [])(set)

        // Separating entity types so a Client isn't compared to ClientService, as this will always return a diff
        const changes = R.pipe(
            R.pluck('entityType'),
            R.uniq,
            R.map(type =>
                R.pipe(
                    R.filter(R.propEq('entityType', type)),
                    formatChanges,
                )(changeSets)
            ),
            R.flatten,
            R.sortBy(R.prop('timestamp')),
        )(changeSets)

        return ok(
            res,
            R.assocPath(['_embedded', 'changes'], changes, {})
        )
    } catch (e) {
        return serverErrorAndReport(res, 'Could not calculate change sets', e)
    }
}

module.exports = {
    calculateFieldChange,
    searchLatestChangeSets,
}
