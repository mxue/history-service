const { getMongoDb } = require('@fispan/commons-node')

const changeSetCollection = () => getMongoDb().collection('changeSets')
const moneyTransferLogsCollection = () => getMongoDb().collection('moneyTransferLogs')
const paymentRequestLogsCollection = () => getMongoDb().collection('paymentRequestLogs')
const paymentStatusLogsCollection = () => getMongoDb().collection('paymentStatusLogs')
const apiRequestLogsCollection = () => getMongoDb().collection('apiRequestLogs')

module.exports = {
    apiRequestLogsCollection,
    changeSetCollection,
    moneyTransferLogsCollection,
    paymentRequestLogsCollection,
    paymentStatusLogsCollection,
}
