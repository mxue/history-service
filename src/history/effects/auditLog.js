const { logger } = require('@fispan/commons-node')
const { changeSetCollection } = require('./collections')

const persistChangeSet = async (changeSet) => {
    try {
        const insertResult = await changeSetCollection().insertOne(changeSet)
        logger.info(`[DB] Persisted change set log ${insertResult.insertedId}`)

        return insertResult.insertedId.valueOf()
    } catch (e) {
        logger.error('[DB] Could not persist change set log\n%o', e)
    }
}

const getLatestChangeSet = async (query) => {
    try {
        const queryResult = await changeSetCollection().findOne(query)

        return queryResult
    } catch (e) {
        logger.error('[DB] Could not find change set\n%o', e)
    }
}

const getLatestChangeSetsWithPaging = async (query, page, size) => {
    try {
        const queryResult = await changeSetCollection()
            .find(query)
            .sort({ timestamp: -1 })
            .skip(size * page)
            .limit(size)
            .toArray()

        return queryResult
    } catch (e) {
        logger.error('[DB] Could not find change set\n%o', e)
    }
}

const getLatestChangeSets = async (query) => {
    try {
        const queryResult = await changeSetCollection()
            .find(query)
            .sort({ timestamp: 1 })
            .toArray()

        return queryResult
    } catch (e) {
        logger.error('[DB] Could not find change set\n%o', e)
    }
}

module.exports = {
    getLatestChangeSet,
    getLatestChangeSets,
    getLatestChangeSetsWithPaging,
    persistChangeSet,
}
