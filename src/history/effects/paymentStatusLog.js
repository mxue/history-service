const { logger } = require('@fispan/commons-node')
const { paymentStatusLogsCollection } = require('./collections')

const persistPaymentStatusLog = async (paymentStatusLog) => {
    try {
        const insertResult = await paymentStatusLogsCollection().insertOne(paymentStatusLog)
        logger.info(`[DB] Persisted payment status log ${insertResult.insertedId}`)

        return insertResult.ops
    } catch (e) {
        logger.error('[DB] Could not persist payment status log\n%o', e)
    }
}

const getLatestPaymentStatusLogs = async (query) => {
    try {
        const queryResult = await paymentStatusLogsCollection().findOne(query)
        logger.info('[DB] Found payment status log')

        return queryResult
    } catch (e) {
        logger.error('[DB] Could not query payment status log\n%o', e)
    }
}

module.exports = {
    getLatestPaymentStatusLogs,
    persistPaymentStatusLog,
}
