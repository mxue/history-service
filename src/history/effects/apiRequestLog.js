const {logger} = require('@fispan/commons-node')
const {apiRequestLogsCollection} = require('./collections')
const {encrypt, decrypt} = require('../utils/fleEncryption')
const {resolveServiceProviderId} = require('../utils/serviceProviderResolver')

const persistAPIRequest = async (requestEvent) => {
    try {
        const encryptedEvent = await encrypt(requestEvent, [
                '$.requestBody',
                '$.responseBody'
            ],
            resolveServiceProviderId(requestEvent))

        const insertResult = await apiRequestLogsCollection().insertOne(encryptedEvent)
        logger.info(`[DB] Persisted API request log ${insertResult.insertedId}`)

        return insertResult.ops
    } catch (e) {
        logger.error('[DB] Could not persist API request log\n%o', e)
    }
}

const searchApiRequestLogs = async (query, page, size) => {
    try {
        const queryResult = await apiRequestLogsCollection()
            .find(query)
            .skip(size * page)
            .limit(size)
            .sort({startTime: -1})    //Default sorting to show latest first
            .toArray()
        logger.info(`[DB] Found ${queryResult.length} api request logs`)

        return await decrypt(queryResult)

    } catch (e) {
        logger.error('[DB] Could not query api request log\n%o', e)
    }
}

module.exports = {
    persistAPIRequest,
    searchApiRequestLogs
}
