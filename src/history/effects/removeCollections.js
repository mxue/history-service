const { logger } = require('@fispan/commons-node')
const {
    changeSetCollection,
    moneyTransferLogsCollection,
    paymentRequestLogsCollection,
    paymentStatusLogsCollection,
} = require('./collections')

module.exports = async () => {
    try {
        const removeResults = await Promise.all([
            changeSetCollection().remove({ }),
            moneyTransferLogsCollection().remove({ }),
            paymentRequestLogsCollection().remove({ }),
            paymentStatusLogsCollection().remove({ }),
        ])
        logger.info('[DB] Removed all documents')

        return removeResults
    } catch (e) {
        logger.error('[DB] Could not remove all documents\n%o', e)
    }
}
