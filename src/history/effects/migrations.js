const Umzug = require('umzug')
const { getMongoDb, logger } = require('@fispan/commons-node')
const { migrationsCollectionName } = require('../../configs')

module.exports = () => new Umzug({
    storage: 'mongodb',
    storageOptions: {
        context: 'default',
        collection: getMongoDb().collection(migrationsCollectionName),
    },

    migrations: {
        params: [
            getMongoDb,
        ],
        path: `${__dirname}/../../migrations`,
        pattern: /\.js$/,
    },

    logging: message => message.indexOf('.gitkeep') === -1 && logger.info(`[DB Migration] ${message}`),
})
