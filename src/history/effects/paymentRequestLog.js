const { logger } = require('@fispan/commons-node')
const { paymentRequestLogsCollection } = require('./collections')
const { encrypt, decrypt } = require('../utils/fleEncryption')

const persistPaymentRequestLog = async (paymentRequestLog) => {
    try {
        const encryptedLog = await encrypt(paymentRequestLog, ['$.statusMessages'],
            paymentRequestLog.serviceProviderId)
        const insertResult = await paymentRequestLogsCollection().insertOne(encryptedLog)
        logger.info(`[DB] Persisted payment request log ${insertResult.insertedId}`)

        return insertResult.ops
    } catch (e) {
        logger.error('[DB] Could not persist payment request log\n%o', e)
    }
}

const searchPaymentRequestLogs = async (query, page, size) => {
    try {
        const queryResult = await paymentRequestLogsCollection()
            .find(query)
            .skip(size * page)
            .limit(size)
            .toArray()
        logger.info(`[DB] Found ${queryResult.length} payment request logs`)

        return await decrypt(queryResult)

    } catch (e) {
        logger.error('[DB] Could not query payment request log\n%o', e)
    }
}

module.exports = {
    persistPaymentRequestLog,
    searchPaymentRequestLogs,
}
