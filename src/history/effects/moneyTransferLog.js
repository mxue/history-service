const { logger } = require('@fispan/commons-node')
const { moneyTransferLogsCollection } = require('./collections')

const persistMoneyTransferLog = async (moneyTransferLog) => {
    try {
        const insertResult = await moneyTransferLogsCollection().insertOne(moneyTransferLog)
        logger.info(`[DB] Persisted money transfer log ${insertResult.insertedId}`)

        return insertResult.ops
    } catch (e) {
        logger.error('[DB] Could not persist money transfer log\n%o', e)
    }
}

const searchMoneyTransferLogs = async (query, page, size) => {
    try {
        const queryResult = await moneyTransferLogsCollection()
            .find(query)
            .sort({ timestamp: -1 })
            .skip(size * page)
            .limit(size)
            .toArray()
        logger.info(`[DB] Found ${queryResult.length} money transfer logs`)

        return queryResult
    } catch (e) {
        logger.error('[DB] Could not query money transfer log\n%o', e)
    }
}

module.exports = {
    persistMoneyTransferLog,
    searchMoneyTransferLogs,
}
