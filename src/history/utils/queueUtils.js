const R = require('ramda')
const { logger, getEventType, getMessageFromEvent } = require('@fispan/commons-node')
const symmetricDifference = require('./symmetricDifference')

const logType = ({ withMessage = false }) => (event) => {
    logger.info('[Event Bus] %o', getEventType(event), withMessage ? getMessageFromEvent(event) : '')
    return event
}

const hasNonExternalIdChanges = oldChangeSet => newChangeSet =>
    R.any(
        R.pipe(
            R.prop('propertyName'),
            R.last,
            R.complement(R.equals('externalId'))
        )
    )(symmetricDifference(oldChangeSet, newChangeSet))

module.exports = {
    getMessageFromEvent,
    hasNonExternalIdChanges,
    logType,
}
