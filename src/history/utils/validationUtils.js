const moment = require('moment')
const R = require('ramda')
const { respondWith: { badRequest } } = require('./responseUtils')

const isDateValid = date => moment(date, 'YYYY-MM-DDTHH:mm:ss.SSS', true).isValid()

const validateDates = queryParamNames => (req, res, next) =>
    R.pipe(
        R.pickAll(R.__, req.query),
        R.values,
        R.all(isDateValid),
    )(queryParamNames)
        ? next()
        : badRequest(
            res,
            `[${queryParamNames}] params date format must be YYYY-MM-DDTHH:mm:ss.SSS, e.g.: 2019-06-04T23:18:28.123`,
        )

const isBlank = R.either(R.isNil, R.isEmpty)

module.exports = {
    isBlank,
    isDateValid,
    validateDates,
}
