const resolveServiceProviderId = (requestEvent) => {
    if(requestEvent && Array.isArray(requestEvent.serviceProviderIds) && requestEvent.serviceProviderIds.length > 0) {
        return requestEvent.serviceProviderIds[0]
    }
    return undefined
}

module.exports = {
    resolveServiceProviderId
}