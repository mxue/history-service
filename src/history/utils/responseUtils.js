const R = require('ramda')
const {
    isClient,
    isOperationsAtLeast,
    isClientOrServiceProviderAtLeast,
    reportIncident,
} = require('@fispan/commons-node')

const bufferToInt = buffer => parseInt(buffer, 10)

const getPageable = req => R.applySpec({
    page: R.pipe(R.path(['query', 'page']), bufferToInt, R.defaultTo(0)),
    size: R.pipe(R.path(['query', 'size']), bufferToInt, R.defaultTo(25)),
})(req)

const ifAuthorized = async (authFn, req, res, next) =>
    authFn(req)
        ? next()
        : res.status(401).json({ message: 'Unauthorized' })

const doAsClient = (req, res, next) =>
    ifAuthorized(isClient, req, res, next)

const doAsOperationsAtLeast = (req, res, next) =>
    ifAuthorized(isOperationsAtLeast, req, res, next)

const doAsClientOrServiceProviderAtLeast = (req, res, next) =>
    ifAuthorized(isClientOrServiceProviderAtLeast, req, res, next)

const respondWith = {
    ok: (res, body) => res.status(200).json(body),
    accepted: (res, message, rest) => res.status(202).json({ message, ...rest }),
    badRequest: (res, message, rest) => res.status(400).json({ message, ...rest }),
    notFound: (res, message) => res.status(404).json({ message }),
    serverError: (res, message, rest) => res.status(500).json({ message, ...rest }),
    serverErrorAndReport: (res, message, error) => {
        reportIncident(message, error)
        return res.status(500).json({ message })
    },
}

const getDeclaredRoutes = (prefix = '', router) =>
    R.pipe(
        R.prop('stack'),
        R.filter(R.has('route')),
        R.map(({ route: { methods, path } }) =>
            `${R.pipe(R.keys, R.join(', '), R.toUpper)(methods)} ${prefix}${path}`),
        R.join('\n\t'),
    )(router)

const jsonPath = path =>
    R.pipe(
        // eslint-disable-next-line no-restricted-globals
        R.reduce((acc, curr) => acc.concat(isNaN(curr) ? `.${curr}` : `[${curr}]`), ''),
        R.drop(1)
    )(path)

module.exports = {
    doAsClient,
    doAsClientOrServiceProviderAtLeast,
    doAsOperationsAtLeast,
    getDeclaredRoutes,
    getPageable,
    jsonPath,
    respondWith,
}
