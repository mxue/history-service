const R = require('ramda')
const { isBlank } = require('./validationUtils')

const maskAccountNumber = R.replace(/.(?=.{4})/g, '*')

const fromArraytoIndexedObject = array =>
    R.unless(
        R.isNil,
        R.pipe(
            R.addIndex(R.map)((val, idx) => R.objOf(idx, val)),
            R.mergeAll,
        )
    )(array)

const formatChangeValue = (value, path) =>
    R.pipe(
        R.defaultTo(''),
        R.includes('accountNumber')(path) ? maskAccountNumber : R.identity
    )(value)

const getValueChange = (oldValue, newValue, path) =>
    R.or(R.all(isBlank, [oldValue, newValue]), R.equals(oldValue, newValue))
        ? null
        : ({
            oldValue: formatChangeValue(oldValue, path),
            newValue: formatChangeValue(newValue, path),
            propertyName: path,
            // eslint-disable-next-line no-nested-ternary
            status: isBlank(oldValue) ? 'added' : R.isEmpty(newValue) ? 'removed' : 'edited',
        })

const symmetricDifference = (oldChangeSet, newChangeSet, path = []) => {
    const keys = R.union(R.keysIn(oldChangeSet), R.keysIn(newChangeSet))
    return R.unless(
        R.isEmpty,
        R.reduce((changes, key) => {
            const values = R.map(R.prop(key))([oldChangeSet, newChangeSet])
            if (R.any(R.or(R.is(Object), R.is(Array)))(values)) {
                return R.concat(
                    symmetricDifference(
                        ...R.when(R.any(R.is(Array)), R.map(fromArraytoIndexedObject))(values),
                        R.append(key, path)
                    ),
                    changes,
                )
            }
            const newChange = getValueChange(...values, R.append(key, path))
            return R.isNil(newChange) ? changes : R.append(newChange, changes)
        }, [])
    )(keys)
}

module.exports = symmetricDifference
