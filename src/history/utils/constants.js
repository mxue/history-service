const EVENT = {
    HISTORY_EVENT: 'com.fispan.event.bus.models.events.HistoryEvent',
    CONFIG_API_MODEL_DELETED: 'com.fispan.event.bus.models.events.Events$ConfigApiModelsDeletedEvent',
    MONEY_TRANSFER_LOG: 'com.fispan.event.bus.models.events.MoneyTransferLogEvent',
    PAYMENT_REQUEST_CHANGED: 'com.fispan.event.bus.models.events.PaymentRequestStatusChangedEvent',
    PAYMENT_STATUS_EVENT: 'com.fispan.event.bus.models.events.PaymentStatusEvent',
    API_REQUEST_LOGGING_EVENT: 'com.fispan.event.bus.models.events.ApiRequestLoggingEvent',
}

module.exports = {
    EVENT,
    QUEUE_PREFETCH_COUNT: 100,
    SEARCH_SIZE: 20,
}
