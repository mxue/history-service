const R = require('ramda')
const { getClientId, getServiceProviderId } = require('@fispan/commons-node')

const withJailing = (request, query = {}) => {
    const jailingTerm = R.converge(R.defaultTo, [
        R.pipe(
            getClientId,
            R.unless(R.isNil, R.assocPath(['clientId', '$eq'], R.__, {})),
        ),
        R.pipe(
            getServiceProviderId,
            R.unless(R.isNil, R.assocPath(['serviceProviderId', '$eq'], R.__, {})),
        ),
    ])(request)

    return R.mergeRight(jailingTerm, query)
}

module.exports = {
    withJailing,
}
