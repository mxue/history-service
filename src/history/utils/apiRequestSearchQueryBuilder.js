const R = require('ramda')

const customMapperFunctions = {
    responseStatus: (queryValue) => {
        return parseInt(queryValue) || queryValue
    },
    requestUrl: (queryValue) => {
        return {$regex: queryValue.startsWith('^') ? queryValue : `^${queryValue}`}
    }
}

const defaultMapperFunction = (queryValue) => {
    return queryValue
}

const whiteListQueryParams = [
    'requestUrl',
    'metadata.type',
    'metadata.clientId',
    'metadata.paymentRequestId',
    'metadata.serviceProviderId',
    'responseStatus',
    'zipkinTraceId',
    'tracerId',
    'requestMethod'
]


const buildQueryString = (queryParams) => {
    const queryStrings = Object.entries(queryParams || {}).filter(([key]) => whiteListQueryParams.includes(key))
        .map(([key, value]) => {
            const mapperFunction = customMapperFunctions[key] || defaultMapperFunction
            return {
                [key]: mapperFunction(value)
            }
        })

    return R.mergeAll(queryStrings)
}

module.exports = {
    buildQueryString
}