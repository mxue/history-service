const {FleClient, KmsEncryptionProvider} = require('@fispan/fle-service')
const {logger} = require('@fispan/commons-node')

let fleClient;

const FLE_ENABLED = (process.env.FLE_ENABLED || 'false').toLowerCase()  === 'true'

if (FLE_ENABLED) {
    const kmsEncryptionProvider = new KmsEncryptionProvider({logger})
    fleClient = new FleClient({encryptionProvider: kmsEncryptionProvider, logger, fallbackDecryptor: () => "{}"})
    logger.info(`FLE: enabling fle on history service with provider: kmsEncryptionProvider`)
} else {
    logger.info('FLE: feature not enabled')
}

const encrypt = async (payload, encryptionPaths, dataKeyReference) => {
    if (FLE_ENABLED) {
        return await fleClient.encryptPayload(payload, encryptionPaths, dataKeyReference)
    } else {
        return payload;
    }
}

const decrypt = async (payload) => {
    if (FLE_ENABLED) {
        if(Array.isArray(payload)) {
            return await Promise.all(payload.map(async (singlePayload) => {
                return await decryptSingle(singlePayload)
            }))
        }
        return await decryptSingle(payload);
    }
    return payload;
}

const decryptSingle = async (payload) =>  {
    try {
        return await fleClient.decryptPayload(payload)
    } catch (e) {
        logger.warn('FLE: error decrypting payload, returning encrypted payload')
        return payload
    }
}

module.exports = {
    encrypt,
    decrypt,
}