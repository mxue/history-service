const R = require('ramda')
const { addMonths } = require('date-fns')

const getTTLDate = () => {
    const TTL = 1 //  in months
    return addMonths(new Date(), TTL)
}

const mapApiRequestLoggingEvent = (event) => {

    const parseDateString = (dateString) => {
        return Date.parse(dateString) ? new Date(dateString) : dateString
    }

    return R.applySpec({
        tracerId: R.prop('tracerId'),
        zipkinTraceId: R.prop('zipkinTraceId'),
        requestHeaders: R.prop('requestHeaders'),
        requestUrl: R.prop('requestUrl'),
        requestMethod: R.prop('requestMethod'),
        requestBody: R.prop('requestBody'),
        responseHeaders: R.prop('responseHeaders'),
        responseStatus: R.prop('responseStatus'),
        responseMessage: R.prop('responseMessage'),
        responseBody: R.prop('responseBody'),
        startTime: R.pipe(R.prop('startTime'), parseDateString),
        endTime: R.pipe(R.prop('endTime'), parseDateString),
        metadata: R.prop('metadata'),
        expireAt: getTTLDate,
        clientIds: R.prop('clientIds'),
        serviceProviderIds: R.prop('serviceProviderIds'),
        roles: R.prop('roles'),
    })(event)
}

module.exports = {
    mapApiRequestLoggingEvent
}