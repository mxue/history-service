FROM node:12.22.5-alpine AS builder
# Create app directory
WORKDIR /app

RUN npm config set "//registry.npmjs.org/:_authToken" "2d499973-a43d-4262-9e73-4c6fe314d816"

# Configure tini
RUN apk add --no-cache tini

COPY package.json ./
COPY package-lock.json ./

RUN npm ci

COPY --chown=node:node . .

# Expose port
EXPOSE 9400

# Set runtime configurations
ENV NODE_ENV=production
ENV APPLICATION_NAME=history-service
USER node

# Execute server
CMD ["node", "src/index.js"]
